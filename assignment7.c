#include <stdio.h>

int main()
{
  
  int a[10][10],i,j,n,di=0,bdi=0,adi=0;
  printf("Enter order of matrix\n");
  scanf("%d",&n);
  printf("Enter the elements\n");
  for(i=0;i<n;i++)
  for(j=0;j<n;j++)
  scanf("%d",&a[i][j]);
  
  for(i=0;i<n;i++)
  for(j=0;j<n;j++)
  {
    if(i==j)
    di=di+a[i][j];
    else if(i<j)
    adi=adi+a[i][j];
    else 
    bdi=bdi+a[i][j];
  }
  printf("Sum of diagonal elements = %d\n",di);
  printf("Sum of elements above diagonal = %d\n",adi);
  printf("Sum of elements below diagonal = %d\n",bdi);
}