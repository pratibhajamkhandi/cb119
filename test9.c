#include <stdio.h>
void swap (int *x,int *y);
int main()
{
    int a,b;
    printf("Enter a & b:\n");
    scanf("%d %d",&a,&b);
    printf("Numbers before swapping\n a=%d b=%d",a,b);
    swap(&a,&b);
    printf("\nSwapped numbers\n a=%d b=%d",a,b);
    return 0;
}
void swap (int *x,int *y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t;
}
